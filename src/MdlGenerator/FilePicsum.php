<?php

declare(strict_types=1);

namespace Uji\MdlGenerator;

class FilePicsum {

    const DEFAULT_SIZE_BYTES = 5*1024*1024;

    public function get($size = self::DEFAULT_SIZE_BYTES) : string {
        return base64_encode(random_bytes($size));
    }
}