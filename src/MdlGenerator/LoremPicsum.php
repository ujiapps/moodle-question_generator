<?php

declare(strict_types=1);

namespace Uji\MdlGenerator;

use GuzzleHttp\Client;

class LoremPicsum {

    private $client;

    public function __construct() {
        $this->client = new Client();
    }

    public function get(): string {
        $resp = $this->client->get("https://picsum.photos/400/600");
        return base64_encode((string) $resp->getBody());
    }
}