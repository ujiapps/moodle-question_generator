<?php

declare(strict_types=1);

namespace Uji\MdlGenerator;

class QuestionText extends \SplFileObject {

    public function __construct(string $filepath) {
        parent::__construct($filepath, "r");
    }

    public function get_text(int $numchars) : string {
        return trim(preg_replace("/\n/", " ", $this->fread($numchars)));
    }
}