<?php

declare(strict_types=1);

namespace Uji\MdlGenerator;

class CmdParser
{

    /**
     * The path to a text file where we'll get text to use in the generated questions.
     *
     * @var string
     */
    private $fromfilepath;

    private const VALID_OPTIONS = [
        'f',
        'from',
        'h',
        'help'
    ];

    /**
     * Indica si imprimir un mensaje de ayuda o no.
     *
     * @var bool
     */
    private $help;

    /**
     * Number of questions.
     *
     * @var int
     */
    private $numberofquestions;

    public function __construct(?string $fromfilepath, bool $help, int $numberofquestions)
    {
        $this->fromfilepath = $fromfilepath;
        $this->help = $help;
        $this->numberofquestions = $numberofquestions;
    }

    public static function parse(array $argv): self
    {
        $options = getopt('f:hn:', ['from:', 'help', 'number'], $undefindex);
        if (!$options) {
            throw new \Exception('Debe especificar alguna opción');
        }

        if (!self::has_passed_a_valid_option($options)) {
            throw new \Exception("No ha pasado una opción válida");
        }

        $from = null;
        $help = false;
        $number = 40;

        if (isset($options['f'])) {
            $from = $options['f'];
        }
        if (isset($options['from'])) {
            $from = $options['from'];
        }
        if (isset($options['h'])) {
            $help = true;
        }
        if (isset($options['help'])) {
            $help = true;
        }
        if (isset($options['n'])) {
            $number = (int) $options['n'];
        }
        if (isset($options['number'])) {
            $number = (int) $options['number'];
        }

        return new self(
            $from,
            $help,
            $number
        );
    }

    private static function has_passed_a_valid_option(array $options): bool
    {
        $passedoptions = array_keys($options);
        $intersect = array_intersect($passedoptions, self::VALID_OPTIONS);
        return count($intersect) > 0;
    }

    public function get_fromfilepath(): string
    {
        return $this->fromfilepath;
    }

    public function get_help(): bool
    {
        return $this->help;
    }

    public function get_number(): int
    {
        return $this->numberofquestions;
    }
}
