<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use Uji\MdlGenerator\CmdParser;
use Uji\MdlGenerator\FilePicsum;
use Uji\MdlGenerator\LoremPicsum;
use Uji\MdlGenerator\QuestionText;

function print_help(): void
{
    global $argv;

    fprintf(STDERR, "Uso: {$argv[0]} -f FICHERO [ -n NUMERO_PREGUNTAS ] [ -h ]\n");
    fprintf(STDERR, "OPCIONES:\n");
    fprintf(STDERR, "    -f, --from     Fichero con texto de origen (lorem ipsum...)\n");
    fprintf(STDERR, "    -n, --number   Número de preguntas a generar\n");
    fprintf(STDERR, "    -h, --help     Imprime este mensaje de ayuda\n\n");
    fprintf(STDERR, "EJEMPLOS:\n");
    fprintf(STDERR, "    Generar 50 preguntas multichoice:");
    fprintf(STDERR, "        php src/gen_multichoice.php -f data/loremipsum.txt -n 50\n\n");
}

$options = CmdParser::parse($argv);

if ($options->get_help()) {
    print_help();
    exit(0);
}
if (!$options->get_fromfilepath()) {
    fprintf(STDERR, "ERROR. Debe especificar un fichero\n");
    print_help();
    exit(1);
}

$filepicsum = new FilePicsum();
$lorempicsum = new LoremPicsum();
$qText = new QuestionText($options->get_fromfilepath());

$numimagen=1;
$numfichero=1;

echo <<<_EOF
<?xml version="1.0" ?>
<quiz>
    <question type="category">
        <category>
            <text>Load Testing Category</text>
        </category>
    </question>

_EOF;

$generated = 0;
while ($generated < $options->get_number()) {
    echo <<<__EOF
    <question type="multichoice">
        <name>
            <text>{$qText->get_text(10)}</text>
        </name>
        <questiontext format="html">
            <text><![CDATA[<p>{$qText->get_text(500)}
            <img src="@@PLUGINFILE@@/imagen_$numimagen.jpg" alt="imagen_$numimagen" width="800" height="400" class="img-responsive atto_image_button_text-bottom">
            <a href="@@PLUGINFILE@@/fichero_$numfichero.data" target="_new">fichero_$numfichero.data</a>
            </p>]]></text>
            <file name="fichero_$numfichero.data" path="/" encoding="base64">{$filepicsum->get(2^20)}</file>
            <file name="imagen_$numimagen.jpg" path="/" encoding="base64">{$lorempicsum->get()}</file>

        </questiontext>

__EOF;

    $numimagen++;

    $fraction = 100;
    for ($i=0; $i < 8; $i++) {
        echo <<<_EOF
        <answer fraction="$fraction">
            <text>{$qText->get_text(10)}</text>
            <feedback><text>{$qText->get_text(100)}</text></feedback>
        </answer>

_EOF;
        $fraction = 0;
    }

    echo <<<_EOF
        <shuffleanswers>1</shuffleanswers>
        <single>true</single>
    </question>

_EOF;

    $generated++;
}

echo "</quiz>";
