# README

This script currenlty generates random multichoice questions in Moodle XML format. It generates:

* Multichoice questions
* Single select answers
* Shuffled answers

Once generate, it can be imported using the Import funcionality of Moodle's question bank.

This script can be useful to stress test quiz. More question types will be added in the future.

The script uses a lorem ipsum text as data input to generate both question and answer texts. It uses [lorem picsum](https://picsum.photos/) in order to put random images.

## Installation

```bash
git clone git@bitbucket.org:ujiapps/moodle-question_generator.git
cd moodle-question_generator
composer install
```

## Use

Generate 50 random multichoice questions:

```bash
php src/gen_multichoice.php -f data/loremipsum.txt -n 50
```

# LICENSE

EUPLv1.2 - Universitat Jaume I <https://www.uji.es/>
